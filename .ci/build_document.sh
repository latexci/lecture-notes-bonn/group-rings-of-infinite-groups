set -e
echo "Building document"
make pdf
mkdir public
mv build/2023_Group_Rings.pdf public
mv build/2023_Group_Rings.log public
cd public/
if ! command -v tree &> /dev/null
then
  echo "No tree utility found, skipping making tree"
else
  tree -H '.' -I "index.html" -D --charset utf-8 -T "Group rings of infinite groups" > index.html
fi
