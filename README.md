# Group rings of infinite groups 

These are the lecture notes for the 'Group rings of infinite groups', taught in winter term 23/24 at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://latexci.gitlab.io/lecture-notes-bonn/group-rings-of-infinite-groups/2023_Group_Rings.pdf
[2]: https://latexci.gitlab.io/lecture-notes-bonn/group-rings-of-infinite-groups/2023_Group_Rings.log
[3]: https://latexci.gitlab.io/lecture-notes-bonn/group-rings-of-infinite-groups/
