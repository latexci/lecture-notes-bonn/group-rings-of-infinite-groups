\lecture[Group rings. Unit, zero-divisor, idempotent and directly finiteness conjectures and their implications. Residually finite groups.]{Di 10 October 2023}{Group rings and their conjectures}

\section{Introduction}

Recommended literature: \cite{passman}.

\begin{definition}
  Let $R$ be a ring and $G$ be a group.
  The \vocab{group ring}
  \[
    R[G] \coloneqq \set{
      \sum_{i=1}^n r_{g_i} \cdot g_i
      \suchthat
      r_{g_i} \in R, g_i \in G
    }
  \]
    is the ring consisting of finite formal $R$-linear combinations
    of groups elements with multiplication
    \[
      \left(\sum r_{g} g\right) \cdot \left(\sum s_{h} h\right)
      \coloneqq 
      \sum_k (\sum_{gh = k} r_g s_h) \cdot k
      .
    \]
\end{definition}

In this course, we will (almost) always have $R =\mathbb{Z}$ or $R = K$ for a field $K$.
In the latter case, one often calls $K[G]$ the group algebra.

\begin{example}
  For $G = \mathbb{Z} = \left< t \right> $,
  $R[G]$ is the ring of Laurent polynomials in $t$ over $\mathbb{R}$,
  usually denoted $R[t, t^{-1}]$.
\end{example}

\begin{remark}
  Noether's viewpoint on this is that Representations of groups are modules
  over group rings.
  We don't need this here, but this is an additional motivation on why
  one would want to study group rings.
\end{remark}

\begin{warning}
  $K[G]$ is non-commutative unless $G$ is abelian.
  It is (left)-Noetherian only in special settings.
  It is \underline{never} semisimple for infinite $G$,
  this is Masuhke's theorem.
\end{warning}

Although group rings tend to have bad ring-theoretic properties,
they conjecturally have nice elementary properties.

Note first that for $r \in R^{\times}$ and $g\in G$,
the element $rg \in R[G]$ is a unit,
since $(rg)^{-1} = r^{-1}g^{-1}$. Such units are called \vocab{trivial}.

\begin{conjecture}[Kaplansky]
  Let $K$ be a field and $G$ be a torsion-free group.
  Then $K[G]$:
  \begin{itemize}
    \item has no non-trivial units.
      That is, if $αβ = βα = 1$, then $α = rg$ for some $r\in R^{\times}$, $g\in G$.
    \item has no non-zero zero divisors.
      That is, if $αβ = 0$, then $α = 0$ or $β = 0$.
    \item has no non-trivial idempotents.
      That is, if $α^2 = α$, then $α = 0$ or $α = 1$.
  \end{itemize}
  For any group $G$, possibly with torsion, $K[G]$
  \begin{itemize}
    \item is directly finite (or von Neumann finite, Dedekind finite).
      This means that elements have left-inverses if and only if they have
      right-inverses.
  \end{itemize}

  These are called the unit, zero-divisor,
  idempotent and direct finiteness conjectures.
\end{conjecture}

\begin{remark}
  The condition of Torsion-freeness is essential.

  Suppose $g$ has order $n \geq 2$,
  then
  \[
    (1 - g) ( 1 + g + g^2 + \ldots + g^{n-1}) = 1 - g^n = 0
  \]
\end{remark}

\begin{remark}
  The unit conjecture is false, the others are open.
\end{remark}

\begin{remark}
  These conjectures are \enquote{local} in the sense that
  they only depend on the finitely generated subgroups of $G$.
\end{remark}


\begin{proposition}
  For a given field $K$ and torsion-free group $G$
  we have
  \[
    \text{unit conjecture}
    \implies
    \text{zero divisor conjecture}
    \implies
    \text{idempotent conjecture}
    \implies
    \text{directly finiteness conjecture}
    .
  \]
\end{proposition}

\begin{proof}
  The weaker $3$ are ring-theoretic statements and easy to verify:

  If $αβ = 1$ and $βα \neq 1$, then
  \[
    (βα)^2 = β(αβ)α = βα
  \]
  is a non-trivial idempotent. Note that $βα \neq 0$
  since for example $(αβ)^2 = 1$.

  If $α$ is a non-trivial idempotent, then
  \[
    α(α-1) = α^2 - α = 0
  \]
  is a non-trivial zero divisor with both factors non-zero by assumption.

  The unit-conjecture is a \enquote{group ring theoretic} statement
  and the proof of the implication
  \[
    \text{unit conjecture} \implies \text{zero-divisor conjecture}
  \]
  requires \nameref{thm:connell}, which we will prove later in the course using
  group theory.

  Since $G$ is torsion-free, $K[G]$ is prime.
  Suppose $αβ = 0$ for $0 \neq α, β \in K[G]$e.
  Then there exists $γ \in K[G]$ with $βγα \neq 0$:
  Otherwise, $(K[G]βK[G])(K[G]αK[G]) = 0 \subset K[G]$.

  Now,
  \[
    (1 - βγα)(1 + βγα) = 1 - (βγα)^2
    = 1 - βγ(αβ)γα = 1
    .
  \]
  Then $1 + βγα$ is a non-trivial unit, since if it were trivial,
  $ 1 + βγα = kg$ and then
  \[
    0 = (βγα)^2 = (kg -1)^2 = k^2g^2 - 2kg + 1
    ,
  \]
  which is absurd unless $g = 1$,
  which forces $βγα = k - 1 \in K$ and then $βγα = 0$, since it is nilpotent.
\end{proof}

\begin{theorem}[Connell]
  \label{thm:connell}
  $K[G]$ is prime
  -- meaning $AB = 0 \implies A = 0$ or $B = 0$
  for two-sided ideals $A$, $B \subset K[G]$ --
  if and only if $G$ has no non-trivial finite normal subgroups.
\end{theorem}

\begin{definition}
  A group $G$ is \vocab{residually finite}
  if for all $1 \neq g \in G$,
  there exists a homomorphism
  \[
    \varphi_g \colon G \to Q
  \]
  with $Q$ finite, such that $\varphi _g(g) \neq 1$.
\end{definition}

We will see later that the direct finiteness conjecture is true for $K = \mathbb{C}$,
for now we will prove:

\begin{proposition}
  Let $G$ be residually finite.
  Then $K[G]$ is directly finite.
\end{proposition}

\begin{notation}
  For $α \in K[G]$, write $(α)_g \in R$ for the coefficient of $g$ in $α$,
  so that
  \[
    α = \sum_g (α)_g \cdot g
  \]
\end{notation}

\begin{proof}
  Suppose $αβ = 1$ in $K[G]$.

  A group homomorphism $\varphi \colon G \to  Q$ induces a ring homomorphism
  $K[G] \to K[Q]$, therefore $K[Q]$ is a $K[G]$-module.

  Note that $Q$ is a basis for the $K$-vector space $K[Q]$,
  so if $Q$ is finite, this is a finite dimensional representation
  of $G$ on $V = K[Q]$.

  Let
  \[
    A \coloneqq  \supp α
    = \set{ g \in G \colon (α)_g \neq 0 } 
  \]
  and $B = \supp β$.

  Let $C = BA$.
  By residual finiteness, there is a finite quotient $\varphi \colon G \twoheadrightarrow Q$ which is injective on $C$:
  Take the product of homomorphisms $\varphi _g$ given by the
  definition for $g\in C^{-1}C \setminus \set{ 1 } $.

  Note this is finite, since $\supp α, \supp β$ are finite.

  Now the induced maps $\varphi_α$, $\varphi_β \in \End(V)$ satisfy $\varphi_α \circ \varphi_β = \id_V$
  and thus - since $V$ is finite-dimensional - we have $\varphi_β \circ \varphi_α = \id_V$.
\end{proof}
