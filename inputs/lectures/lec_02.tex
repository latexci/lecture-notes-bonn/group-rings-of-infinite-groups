%! TEX root = ../../master.tex
\lecture[The (two) Unique Product properties. Unit-conjecture for groups with unique products. Left-orderings and their characterizations. Existence of left-orderings under various assumptions.]{Do 12 Okt 2023}{The unit conjecture}

There is only one known way of proving the unit conjecture,
which we will study today.

\begin{definition}
  \label{def:unique-product-property}
  A group $G$ has the \vocab{unique product property}
  (we will also say that $G$ \enquote{has unique products} or \enquote{has UP})
  if for all nonempty finite sub\emph{sets} $A$, $B \subset G$,
  there exists some element $g\in G$ such that $g = ab$ for
  a unique pair $(a,b) \in A \times B$.
\end{definition}

\begin{example}
  Consider the integers $G = (\mathbb{Z}, +)$.
  Then the product $\max(A) + \max(B)$ is uniquely the sum of elements in $A$ and $B$.
\end{example}

\begin{remark}
  A group with this product is necessarily torsion-free:
  If $1 \neq H \subset G$ is finite,
  then taking $A = B = H$, then every element in $H$ can be written in $\abs{H}$
  ways as the product of two elements of $H$.
\end{remark}

\begin{remark}
  It is difficult to produce torsion free groups that do not have unique products.
\end{remark}

\begin{proposition}
  \label{prop:up-implies-zero-divisor-conjecture}
  A group with the unique product property satisfies the zero-divisor conjecture.
\end{proposition}

\begin{proof}
  Let $α$, $β\in K[G]$ with $α$, $β \neq 0$ and set $A = \supp α$ and $B = \supp β$.
  Write
  \[
    α = \sum_{a\in A} λ_a \cdot a,
    \qquad
    β = \sum_{b\in B} μ_b \cdot b
  \]
  Then if $g = a_0b_0$ for $a\in A$, $b\in B$ is a unique product for $A$ and $B$,
  then we have
  \[
    (αβ)_g = \sum_{ab = g} λ_a μ_b = λ_{a_0} μ_{b_0} \neq 0
    ,
  \]
  so $αβ \neq 0$.
\end{proof}

\begin{remark}
  For the unit conjecture, we need something that is a priori stronger,
  since it could be the case that the non-zero coefficient that we just found is
  just $1$ and all others vanish, therefore $αβ = 1$ cannot be ruled out with the above
  argument.
\end{remark}

\begin{definition}
  \label{def:two-unique-products-property}
  A group $G$ has the \vocab{two unique products property},
  if for all finite subsets $A$, $B \subset G$ with $\abs{A} \abs{B} \geq 2$,
  there exists $g_0 \neq g_1$ such that $g_0 = a_0b_0$ and $g_1 = a_1b_1$
  for unique pairs $(a_0, b_0)$ and $(a_1,b_1)\in A\times B$. 
\end{definition}

\begin{proposition}[Strojnowski]
  The two unique products property is equivalent to the unique products property.
\end{proposition}

\begin{proof}
  \enquote{$\implies$} is immediate (in the case $\abs{A} = 1$ or $\abs{B} = 1$,
  all products are unique).

  \enquote{$\impliedby$}.
  Suppose that $G$ has the unique property but that $A$, $B \subset G$
  are finite sets with $\abs{A} \abs{B} \geq 2$
  have only $1$ unique product.

  Without loss of generality, by translating $A$ on the left by $0^{-1}$ and $B$ on the right
  by $b_0^{-1}$,
  we can assume that $1 = 1 \cdot 1$ is the unique unique product.

  Now let $C = B^{-1}A = \set{ b^{-1}a \suchthat b\in B, a\in A } $
  and let $D = BA^{-1}$.

  We now claim that the pair $(C,D)$ has no unique product.
  Every element of $CD$ can be written as $b_1^{-1}a_1b_2a_2^{-1}$
  for some $a_1$, $a_2 \in A$ and $b_1$, $b_2\in B$.

  If $(a_1, b_2) \neq (1,1)$,
  then by assumption there is another pair $(a_1', b_2')$ with the same product
  and thus $(b_1^{-1}a_1)(b_2a_2^{-1}) = (b_1^{-1}a_1')(b_2'a_2^{-1})$
  is not a unique product.

  If $(a_1, b_2) = (1,1)$,
  then unless $(a_2, b_1) = (1,1)$, we can write
  $b_1^{-1} a_2^{-1} = (a_2b_1)^{-1} = (a_2'b_1')^{-1} = (b_1')^{-1} (a_2')^{-1}$.

  Finally, if $(a_2, b_1) = (1,1)$ and $(a_1,b_2) = (1,1)$,
  then our element of $(C,D)$ is
  $1 = 1 \cdot 1 = b^{-1} \cdot 1 \cdot b \cdot 1^{-1} = 1^{-1}\cdot a\cdot 1\cdot a^{-1}$
  for any $a\in A$ and $b\in B$,
  at least one possibility of which is distinct from $1$.
\end{proof}

\begin{corollary}
  A group with the unique product property satisfies the unit conjecture.
\end{corollary}

\begin{proof}
  Exercise similar to \autoref{prop:up-implies-zero-divisor-conjecture}.
\end{proof}

Now, of course, we would like to find groups that have the unique product property.
Most examples of groups with unique products are left-orderable:

\begin{definition}
  A group $G$ is \vocab{left-orderable} if it admits a total order \enquote{$<$}
  that is left-invariant, i.e.~if $g < h$, then $kg < kh$ for all $g$, $h \in k\in G$.
\end{definition}

\begin{remark}
  No finite group can be left-orderable.
  Say $g ^n = 1$ and $g > 1$. Then $g^2 > g$ and therefore inductively
  \[
    1 = g^n > g^{n-1} > \dotsb > 1
    .
  \]
\end{remark}

\begin{remark}
  Being left- and right-orderable are equivalent:
  Define $g \prec h :\iff g^{-1} \prec h^{-1}$.

  However, admitting a bi-invariant total order is much stronger.
\end{remark}

\begin{proposition}
  \label{prop:left-orderable-group-has-unique-products}
  A left-orderable group has the unique product property.
\end{proposition}

\begin{proof}
  Fix a left-order on $G$.
  We show that the maximum of $AB$ is a unique product.

  Let $b_0 \coloneqq \max B$.
  Then for all $a\in A$, $b\in B \setminus \set{ b_0 } $, 
  we have
  \[
    b < b_0 \implies ab < ab_0
  \]
  Thus, the maximum of $AB$ can only be written in as $a\cdot b_0$
  and must therefore be unique, as
  $a_i \neq a_j \implies a_i \cdot b_0 \neq a_j \cdot b_0$.
\end{proof}

\begin{warning}
  It is \emph{not} necessarily true that $\max(AB) = \max (A) \cdot \max(B).$
\end{warning}

\begin{definition}
  For a left-ordered group $(G, <)$, the set
  \[
    \mathcal{P} \coloneqq \set{ g\in G\colon  g > 1 }
  \]
  is called its \vocab{positive cone}.  
\end{definition}

\begin{fact}
  \label{fact:positive-cone-properties}
  The positive cone satisfies:
  \begin{enumerate}[h]
    \item $\mathcal{P}^2 \subset \mathcal{P}$ (i.e.~ its a subsemigroup).
    \item $G = \mathcal{P} \sqcup \set{ 1 } \sqcup \mathcal{P}^{-1}$.
  \end{enumerate}
\end{fact}

\begin{lemma}
  \label{lm:characterisation-of-left-order-by-positive-cone}
  Left-orders are equivalent to choices of $\mathcal{P}$
  satisfying $1)$ and $2)$ of \autoref{fact:positive-cone-properties}.
\end{lemma}

\begin{proof}
  Exercise. (Hint: $h > g \iff g^{-1} h > 1$).
\end{proof}

\begin{lemma}
  \label{lm:local-characterization-of-left-ordering}
  A group $G$ is left-orderable if and only if
  for all choices $g_1, \dotsc, g_n \in G \setminus \set{ 1 } $,
  there exists a choice of signs $ε_1, \dotsc, ε_n \in \set{ 1, -1 } $
  such that
  \[
    1 \not\in S(g_1^{ε_1}, \dotsc, g_n^{ε_n}),
  \]
  the subsemigroup generated by $g_1^{ε_1}, \dotsc, g_n^{ε_n}$.
\end{lemma}

\begin{proof}
  \enquote{$\implies$}.
  Fix a left-ordering and set $ε_i = 1$ if $g_i \in \mathcal{P}$,
  otherwise $ε_i = -1$.

  \enquote{$\impliedby$}. We use compactness
  (slogan: the inverse limit of nonempty finite sets is nonempty).

  Let $X = \set{ 1, -1 } ^{G \setminus \set{ 1 } }$
  be the set of functions $G \setminus \set{ 1 } \to \set{ 1, -1 } $
  and let $A\subset X$ be the set of those functions that define a positive cone.

  This is equivalent to satisfying (simultaneously!) the condition of choice of sign
  for all possible $g_1, \dotsc, g_n \in G\setminus \set{ 1 } $
  (actually, $n = 3$ suffices).

  That is, if we denote such functions $A_{\set{ g_1, \dotsc, g_n } } \subset X$,
  then
  \[
    A = \bigcap_{\text{$S \subset G$ finite}} A_S
    .
  \]
  But $X$ is compact by Tychonoff's theorem
  and the $A_S$ are closed (they only depend on the restriction to a finite subset)
  and have all finite intersections nonempty.
  Thus, $A$ is nonempty.
\end{proof}

\begin{remark}
  Notice that in the proof, we have implicitly used the axiom of choice
  (or at least some weaker version of it).
  In fact, there are models of Zermelo-Freankel where the previous lemma fails to hold.
\end{remark}

We apply the lemma to prove:

\begin{theorem}[Burns-Hale, 1972]
  Let $G$ be a group.
  If every non-trivial finitely generated subgroup of $G$ has
  a non-trivial left-orderable quotient,
  then $G$ is left-orderable.
\end{theorem}

In particular, a \vocab{locally indicable} group,
i.e.~where every non-trivial finitely generated subgroup surjects onto $\mathbb{Z}$,
is left-orderable.

\begin{corollary}[Higman, 1940]
  Locally indicable groups satisfy the unit and zero-divisor conjectures.
\end{corollary}

\begin{proof}
  Locally indicable $\implies$ left-orderable $\implies$ unique products
  $\implies$ conjecture.
\end{proof}
